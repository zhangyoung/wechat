# wechat

#### 介绍
微信支付
已经集成springboot-starter
#### 软件架构
springboot2.0+


#### 安装教程

    ####pom依赖：
        <dependency>
            <groupId>vip.zywork.wechat</groupId>
            <artifactId>zywechatpay-spring-boot-starter</artifactId>
            <version>0.0.1</version>
        </dependency>

#### 使用说明
                 
    ####yml文件中配置，：
                spirng:
                   zy:
                    wechat-pay:
                      enable: true
                wechat:
                    pay:
                        key: #KEY
                        appId: #APPID
                        mchId: #商户号
                        notifyUrl: #支付回调接口路径
                        certPath: #证书绝对路径 
                        sendBox: false
                        secret: #密钥
                //下单步骤：
                    @Autowired
                    private WXPayClient wxPayClient;
                //第一步：构建下单请求对象
		WXPayRequest wxPayRequest = UnifiedOrderModel.newInstance()
				.setOut_trade_no(requestParam.getTradeNo())
				.setTotal_fee(requestParam.getMoney())
				.setTrade_type(WXPayConfig.APP)
				.setNotifyUrl(notifyUrl)
				.setAttach(attach)
				.setTimeStart(timeStart)
				.setTimeExpire(timeExpire)
				.getRequest();
                //第二步:请求微信下单接口，返回请求结果对象
		WXPayResult wxPayResult = wxPayClient.execute(wxPayRequest, WXPayResult.class);

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
 
