package vip.zywork.datascope.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import vip.zywork.datascope.interceptor.MyDataScopeInnerInterceptor;
import vip.zywork.datascope.interceptor.MyDataScopeInterceptor;
import vip.zywork.datascope.interceptor.MyInterceptor;
import vip.zywork.datascope.support.MyDataScopeHandler;
import vip.zywork.datascope.support.MyDataScopeHandlerSupport;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/5 17:57
 * @since
 */
@EnableConfigurationProperties(MyDataScopeProperties.class)
public class MyDataScopeConfig {


    @Bean
    @ConditionalOnMissingBean(MyDataScopeHandler.class)
    public MyDataScopeHandler getMyDataScopeHandler(MyDataScopeProperties myDataScopeProperties){
        return new MyDataScopeHandlerSupport(myDataScopeProperties);
    }

    @Bean
    @ConditionalOnMissingBean(MyInterceptor.class)
    public MyDataScopeInnerInterceptor getMyDataScopeInnerInterceptor(MyDataScopeHandler myDataScopeHandler){
        return new MyDataScopeInnerInterceptor(myDataScopeHandler);
    }

    @Bean
    @ConditionalOnMissingBean(MyDataScopeInterceptor.class)
    public MyDataScopeInterceptor getMyDataScopeInterceptor(MyInterceptor myDataScopeInnerInterceptor){
        return new MyDataScopeInterceptor(myDataScopeInnerInterceptor);
    }
}
