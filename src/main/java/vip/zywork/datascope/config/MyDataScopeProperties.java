package vip.zywork.datascope.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/5 19:54
 * @since
 */
@ConfigurationProperties(prefix = "spring.zywork.datascope")
public class MyDataScopeProperties {

    /**
     * 配置不进行多租户隔离的表名
     */
    private List<String> ignoreTables = new ArrayList<>();

    public List<String> getIgnoreTables() {
        return ignoreTables;
    }

    public void setIgnoreTables(List<String> ignoreTables) {
        this.ignoreTables = ignoreTables;
    }
}
