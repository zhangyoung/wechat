package vip.zywork.datascope.support;

import com.alibaba.ttl.TransmittableThreadLocal;

/**
 * <p>参数上下文</p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/6 15:51
 * @since
 */
public class DadaScopeParameterHolder {

    private static final ThreadLocal<Object> CONTEXT = new TransmittableThreadLocal<>();

    public static void setDadaScopeParameterHolder(Object object) {
        CONTEXT.set(object);
    }

    public static Object getDadaScopeParameterHolder() {
        return CONTEXT.get();
    }

    public static void clear() {
        CONTEXT.remove();
    }
}
