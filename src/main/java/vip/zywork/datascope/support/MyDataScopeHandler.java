package vip.zywork.datascope.support;


import vip.zywork.datascope.model.MyDataScopeParameter;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/9/6 9:42
 * @since
 */
public interface MyDataScopeHandler {


    /**
     * 是否 管理员
     * @return
     */
    default boolean isAdmin(){return false;}

    /**
     * 获取部门字段名
     * <p>
     * 默认字段名叫: dept_id
     *
     * @return 部门字段名
     */
    default String getDeptIdColumn() {
        return "dept_id";
    }

    /**
     * 根据表名判断是否忽略拼接部门条件
     * <p>
     * 默认都要进行解析并拼接部门条件
     *
     * @param tableName 表名
     * @return 是否忽略, true:表示忽略，false:需要解析并拼接多租户条件
     */
    default boolean ignoreTable(String tableName) {
        return false;
    }
    /**
     * 获取参数值，只支持集合，单个
     * @return 参数值
     */
    MyDataScopeParameter getParameter();
}
