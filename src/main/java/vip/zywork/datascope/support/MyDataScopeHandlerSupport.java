package vip.zywork.datascope.support;

import vip.zywork.datascope.config.MyDataScopeProperties;
import vip.zywork.datascope.model.MyDataScopeParameter;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/5 18:11
 * @since
 */
public class MyDataScopeHandlerSupport implements MyDataScopeHandler {

    private MyDataScopeProperties myDataScopeProperties;

    public MyDataScopeProperties getMyDataScopeProperties() {
        return myDataScopeProperties;
    }

    public void setMyDataScopeProperties(MyDataScopeProperties myDataScopeProperties) {
        this.myDataScopeProperties = myDataScopeProperties;
    }

    public MyDataScopeHandlerSupport(MyDataScopeProperties myDataScopeProperties) {
        this.myDataScopeProperties = myDataScopeProperties;
    }

    /**
     * 根据表名判断是否进行过滤
     * @param tableName
     * @return
     */
    @Override
    public boolean ignoreTable(String tableName) {
        return myDataScopeProperties.getIgnoreTables().stream().anyMatch((e) -> e.equalsIgnoreCase(tableName));
    }

    /**
     * 获取参数值表达式，只支持集合，单个
     * <p>
     *
     * @return 参数值表达式表达式
     */
    @Override
    public MyDataScopeParameter getParameter(){
        return null;
    }
}
