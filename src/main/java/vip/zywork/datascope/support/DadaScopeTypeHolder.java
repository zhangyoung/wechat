package vip.zywork.datascope.support;

import com.alibaba.ttl.TransmittableThreadLocal;
import vip.zywork.datascope.enums.MyDataScopeType;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/9/12 17:58
 * @since
 */
public class DadaScopeTypeHolder {

    private static final ThreadLocal<MyDataScopeType> CONTEXT = new TransmittableThreadLocal<>();

    public static void setDadaScopeType(MyDataScopeType dadaScopeType) {
        CONTEXT.set(dadaScopeType);
    }

    public static MyDataScopeType getDadaScopeType() {
        return CONTEXT.get();
    }

    public static void clear() {
        CONTEXT.remove();
    }
}
