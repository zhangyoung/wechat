package vip.zywork.datascope.annotation;

import java.lang.annotation.*;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/9/12 14:53
 * @since
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Repeatable(MyDataScope.class)
public @interface MyDataColumn {
    String alias() default "";

    String name() default "dept_id";
}
