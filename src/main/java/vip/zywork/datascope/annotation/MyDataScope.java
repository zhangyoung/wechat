package vip.zywork.datascope.annotation;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Table;
import vip.zywork.datascope.enums.MyDataScopeType;
import vip.zywork.datascope.interceptor.AbstractMyDataScopeInterceptor;
import vip.zywork.datascope.support.MyDataScopeHandlerSupport;

import java.lang.annotation.*;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/9/12 14:48
 * @since
 */
@Documented
@Inherited
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyDataScope {

    /**
     * 权限范围，优于{@link MyDataScopeHandlerSupport#getParameter()}中获取,使用默认的话还是从参数中获取
     * @return
     */
    MyDataScopeType scope()default MyDataScopeType.ALL;

    /**
     * 自定义权限范围，如果该值不为空，优于scope，所以必须重写，必须重写，必须重写
     * {@link AbstractMyDataScopeInterceptor#builderExpression(Expression, Table, Object)}方法，
     * 并注入到容器内，第三个参数的 object就是该值，然后判断去拼接sql语句，
     * @return
     */
    String customScope()default "";

    /**
     * 暂未启用
     * @return
     */
    MyDataColumn[] value() default {};

    /**
     * 是否忽略权限范围，实际并无多大意义
     * @return
     */
    boolean ignore() default false;
}
