package vip.zywork.datascope.annotation;

import org.springframework.context.annotation.Import;
import vip.zywork.datascope.config.MyDataScopeConfig;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/5 17:35
 * @since
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({MyDataScopeConfig.class,})
public @interface EnableMyDataScope {
}
