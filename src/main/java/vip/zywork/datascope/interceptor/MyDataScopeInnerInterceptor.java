package vip.zywork.datascope.interceptor;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.Parenthesis;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.expression.operators.relational.InExpression;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import vip.zywork.datascope.enums.MyDataScopeType;
import vip.zywork.datascope.exception.ExceptionUtils;
import vip.zywork.datascope.model.MyDataScopeParameter;
import vip.zywork.datascope.support.MyDataScopeHandler;

import java.util.Objects;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/5 17:20
 * @since
 */
public class MyDataScopeInnerInterceptor extends AbstractMyDataScopeInterceptor {


    public MyDataScopeInnerInterceptor(MyDataScopeHandler dataScopeHandler) {
        super(dataScopeHandler);
    }

    /**
     * 处理条件
     * @param currentExpression
     * @param fromTable
     * @return
     */
    @Override
    public Expression builderExpression(Expression currentExpression, Table fromTable,Object object) {
        MyDataScopeParameter myDataScopeParameter = super.getDataScopeHandler().getParameter();
        Object parameter = myDataScopeParameter.getParameter();

        MyDataScopeType type = (MyDataScopeType)myDataScopeParameter.getMyDataScopeType();
        if (Objects.isNull(parameter) && type.compareTo(MyDataScopeType.ALL) != EQUAL){
            throw ExceptionUtils.mpe("数据权限的参数不能为空！");
        }
        if (object instanceof MyDataScopeType
                && type.compareTo(MyDataScopeType.ALL) != EQUAL){
            type = (MyDataScopeType) object;
        }
        if (object instanceof String){
            throw ExceptionUtils.mpe("如您在@MyDataScope注解中配置了customScope，请重写AbstractMyDataScopeInterceptor的builderExpression方法，并注入到spring容器内");
        }

        Column aliasColumn = this.getAliasColumn(fromTable);
        boolean flag = false;
        switch (type){
            case ALL:
                return null;

            case CUSTOM:
                flag = true;
                break;

            case THIS_DEPARTMENT:
                EqualsTo equalsTo = new EqualsTo();
                equalsTo.setLeftExpression(aliasColumn);
                if (parameter instanceof StringValue){
                    StringValue stringValue = (StringValue)parameter;
                    equalsTo.setRightExpression(stringValue);
                }else {
                    throw ExceptionUtils.mpe("数据权限的参数类型有误！");
                }

                if (currentExpression == null) {
                    return equalsTo;
                }
                if (currentExpression instanceof OrExpression) {
                    return new AndExpression(new Parenthesis(currentExpression), equalsTo);
                } else {
                    return new AndExpression(currentExpression, equalsTo);
                }

            case DEPARTMENT_FOLLOWING:
                flag = true;
                break;
            default:
                return null;
        }
        if (flag){
            if (parameter instanceof ItemsList){
                ItemsList  itemsList = (ItemsList)parameter;
                InExpression inExpression = new InExpression(aliasColumn,itemsList);
                if (null == currentExpression) {
                    // 不存在 where 条件
                    return new Parenthesis(inExpression);
                } else {
                    // 存在 where 条件 and 处理
                    return new AndExpression(currentExpression, inExpression);
                }
            }

        }
        return null;
    }





}
