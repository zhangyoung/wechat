package vip.zywork.datascope.model;

import vip.zywork.datascope.enums.MyDataScopeType;
import vip.zywork.datascope.exception.ExceptionUtils;

import java.util.Objects;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/6 17:24
 * @since
 */
public class MyDataScopeParameter {

    private MyDataScopeType myDataScopeType;
    private Object parameter;
    public MyDataScopeType getMyDataScopeType() {
        return myDataScopeType;
    }

    public Object getParameter() {
        return parameter;
    }

    public MyDataScopeParameter(MyDataScopeType myDataScopeType, Object parameter) {
        this.myDataScopeType = myDataScopeType;
        Class parameterClass = myDataScopeType.getParameterClass();
        if (Objects.nonNull(parameter)){
            if (!parameter.getClass().equals(parameterClass)){
                throw ExceptionUtils.mpe("数据类型的参数有误！！");
            }
            this.parameter = parameter;
        }
    }
    public MyDataScopeParameter( Object parameter) {
        this.parameter = parameter;
    }
}