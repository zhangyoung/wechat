package vip.zywork.datascope.exception;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/5 16:48
 * @since
 */
public class MyDataScopeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public MyDataScopeException(String message) {
        super(message);
    }

    public MyDataScopeException(Throwable throwable) {
        super(throwable);
    }

    public MyDataScopeException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
