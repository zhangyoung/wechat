package vip.zywork.datascope.exception;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/5 16:49
 * @since
 */
public class ExceptionUtils {
    private ExceptionUtils() {
    }

    /**
     * 返回一个新的异常，统一构建，方便统一处理
     *
     * @param msg 消息
     * @param t   异常信息
     * @return 返回异常
     */
    public static MyDataScopeException mpe(String msg, Throwable t, Object... params) {
        return new MyDataScopeException(String.format(msg, params), t);
    }

    /**
     * 重载的方法
     *
     * @param msg 消息
     * @return 返回异常
     */
    public static MyDataScopeException mpe(String msg, Object... params) {
        return new MyDataScopeException(String.format(msg, params));
    }

    /**
     * 重载的方法
     *
     * @param t 异常
     * @return 返回异常
     */
    public static MyDataScopeException mpe(Throwable t) {
        return new MyDataScopeException(t);
    }

    public static void throwMpe(boolean condition, String msg, Object... params) {
        if (condition) {
            throw mpe(msg, params);
        }
    }
}
