package vip.zywork.datascope.enums;

import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;

/**
 * <p>  </p>
 *
 * @author ZHANG_YANG
 * @version 1.0
 * @date 2021/10/6 14:42
 * @since
 */
public enum  MyDataScopeType {

    /**
     * 全部
     */
    ALL("1",null),
    /**
     * 自定义
     */
    CUSTOM("2", ExpressionList.class),
    /**
     * 本部门
     */
    THIS_DEPARTMENT("3",StringValue.class),
    /**
     * 本部门及以下
     */
    DEPARTMENT_FOLLOWING("4", ExpressionList.class),
    ;

    private String type;
    private Class aClass;

    MyDataScopeType(String type, Class aClass) {
        this.type = type;
        this.aClass = aClass;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Class getParameterClass() {
        return aClass;
    }

    public void setaClass(Class aClass) {
        this.aClass = aClass;
    }

    public static MyDataScopeType typeOf(String type) {
        for (MyDataScopeType myDataScopeType : values()){
            if (myDataScopeType.type.equals(type)){
                return myDataScopeType;
            }
        }
        return null;
    }


}
