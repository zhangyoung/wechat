package vip.zywork.wechat;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ComponentScan;
import vip.zywork.wechat.pay.core.WeChatPayProperties;

/**
 * Created date on 2018/11/19
 * Author Zy
 */
@Configuration
@ConditionalOnProperty(prefix = "spring.zywork.wechat-pay", name = "enable",
        havingValue = "true", matchIfMissing = false)
@ComponentScan
@EnableConfigurationProperties({WeChatPayProperties.class})
public class WeChatConfig {
    public WeChatConfig() {
    }
}
