package vip.zywork.wechat.pay.annotations;


import org.springframework.context.annotation.Import;
import vip.zywork.wechat.WeChatConfig;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(WeChatConfig.class)
@Deprecated
public @interface EnableWeChatPay {
}
