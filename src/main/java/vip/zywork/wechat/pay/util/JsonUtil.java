package vip.zywork.wechat.pay.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;

import java.util.Map;


/**
 * Created date on 2019/6/3
 * Author Zy
 */
public class JsonUtil {

  public final static Gson gson = new Gson();

  public static String objectToJson(Object object) {
    return gson.toJson(object);
  }

  public static <T> T jsonToObject(String json, Class<T> object) {
    return gson.fromJson(json, object);
  }
  public static Map jsonToMap(String json){
    return (Map) JSON.parse(json);
  }
  public static <T> T mapToObject(Map map, Class<T> object){
    return  jsonToObject(objectToJson(map),object);
  }
  public static String formartJson(String jsonString, String key){
    JSONObject jObject = new JSONObject(jsonToMap(jsonString));
    return (String) jObject.get(key);
  }
}
