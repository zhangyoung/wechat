package vip.zywork.wechat.pay.util;

import org.apache.commons.lang3.Validate;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;


/**
 * Created date on 2019/6/3
 * Author Zy
 */
public class DateUtil {

  public static final String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";
  public static final String FORMAT_DATE = "yyyy-MM-dd";

  public static String formatDateTime(Date date) {
    if (date == null) return null;
    SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATETIME);
    sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
    return sdf.format(date);
  }

  public static String formatDate(Date date) {
    if (date == null) return null;
    SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
    sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
    return sdf.format(date);
  }

  public static String formatDateTime(Date date, String style) {
    if (date == null) return null;
    SimpleDateFormat sdf = new SimpleDateFormat(style);
    sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
    return sdf.format(date);
  }

  /**
   * 字符串转时间
   *
   * @param dateString dateString
   * @param style style
   * @return Date
   */
  public static Date string2Date(String dateString, String style) {
    if (StringUtils.isEmpty(dateString)) return null;
    Date date = new Date();
    SimpleDateFormat strToDate = new SimpleDateFormat(style);
    try {
      date = strToDate.parse(dateString);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date;
  }

  /**
   * 判断传入的时间是否在当前时间之后，返回boolean值
   * true: 过期
   * false: 还没过期
   * @param date 日期
   * @return boolean
   */
  public static boolean isExpire(Date date) {
    if (date.before(new Date())) return true;
    return false;
  }

  // 获取 hour 后的时间
  public static Date getHourAfter(Date date, int hour) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + hour);
    return calendar.getTime();
  }

  // 获取 hour 前的时间
  public static Date getHourBefore(Date date, int hour) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - hour);
    return calendar.getTime();
  }

  public static Date getDateBefore(Date date, int day) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DAY_OF_MONTH, -day);
    return calendar.getTime();
  }

  public static Date getDateAfter(Date date, int day) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.DAY_OF_MONTH, day);
    return calendar.getTime();
  }

  public static Date getMinuteAfter(Date date, int minute) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.MINUTE, minute);
    return calendar.getTime();
  }

  public static Date getMinuteBefore(Date date, int minute) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(Calendar.MINUTE, -minute);
    return calendar.getTime();
  }

  /**
   *
   * <p>两日期之间的天数差：date1-date2。</p>
   *
   * @param date1 日期
   * @param date2 日期
   * @return 有符号天数差
   */
  public static int getDaysDiff(Date date1, Date date2) {

    int result = 0;

    final Calendar calendar1 = Calendar.getInstance();
    final Calendar calendar2 = Calendar.getInstance();
    calendar1.setTime(date1);
    calendar2.setTime(date2);

    final int flag = calendar1.compareTo(calendar2);

    if (flag < 0) {
      calendar1.setTime(date2);
      calendar2.setTime(date1);
    }

    result = calendar1.get(Calendar.DAY_OF_YEAR) - calendar2.get(Calendar.DAY_OF_YEAR);

    //不在同一年
    while (calendar1.get(Calendar.YEAR) > calendar2.get(Calendar.YEAR)) {

      result += calendar2.getActualMaximum(Calendar.DAY_OF_YEAR);
      calendar2.add(Calendar.YEAR, 1);
    }

    result = flag < 0 ? -result : result;
    return result;
  }


  public static int getDaysDiff(Calendar calendar, Date date) {

    return getDaysDiff(calendar.getTime(), date);
  }

  public static boolean nowIsAfter(Date date1) throws ParseException {
    Date now = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
    date1 = sdf.parse(sdf.format(date1));
    now = sdf.parse(sdf.format(now));
    if(now.after(date1)) {
      return true;
    } else {
      return false;
    }
  }
  public static boolean nowIsAfter(Date date1,String format) throws ParseException {
    Date now = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat(format);
    date1 = sdf.parse(sdf.format(date1));
    now = sdf.parse(sdf.format(now));
    if(now.after(date1)) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * 获取相对于 time 指定的时间（天）
   *
   * @param time         相对的时间
   * @param numberOfDays 改变的时间
   * @return yyyy-MM-dd
   */
  public static String getDesignationTime(Date time, int numberOfDays) {
    //取时间
    Date date = time;
    Calendar calendar = new GregorianCalendar();
    calendar.setTime(date);
    //把日期往后增加一天.整数往后推,负数往前移动
    calendar.add(Calendar.DATE, numberOfDays);
    //这个时间就是日期往后推一天的结果
    date = calendar.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:dd");
    String dateString = formatter.format(date);

    return dateString;
  }

  public static String getDesignationTime1(Date time, int numberOfDays) {
    //取时间
    Date date = time;
    Calendar calendar = new GregorianCalendar();
    calendar.setTime(date);
    //把日期往后增加一天.整数往后推,负数往前移动
    calendar.add(Calendar.DATE, numberOfDays);
    //这个时间就是日期往后推一天的结果
    date = calendar.getTime();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    String dateString = formatter.format(date);

    return dateString;
  }

  /**
   * 判断日期是否是本月
   * @param date 日期
   * @return  boolean
   */
  public static boolean isThisMonth(Date date){
    return isThisTime(date,"yyyy-MM");
  }

  public static boolean isThisWeek(Date date) {
    Calendar calendar = Calendar.getInstance();
    int currentWeek = calendar.get(Calendar.WEEK_OF_YEAR);
    calendar.setTime(date);
    int paramWeek = calendar.get(Calendar.WEEK_OF_YEAR);
    if (paramWeek == currentWeek) {
      return true;
    }
    return false;
  }
  public static boolean isToday(Date date) {
    return isThisTime(date, FORMAT_DATE);
  }


  public static boolean isThisTime(Date date, String pattern) {
    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    String param = sdf.format(date);//参数时间
    String now = sdf.format(new Date());//当前时间
    if (param.equals(now)) {
      return true;
    }
    return false;
  }

  private static Date dateAdd(Date date,int type,int variate){
    validateDateNotNull(date);
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.add(type, variate);
    return calendar.getTime();
  }
  public static Date addYears(Date date, int amount) {
    return dateAdd(date, 1, amount);
  }

  public static Date addMonths(Date date, int amount) {
    return dateAdd(date, 2, amount);
  }

  public static Date addWeeks(Date date, int amount) {
    return dateAdd(date, 3, amount);
  }

  public static Date addDays(Date date, int amount) {
    return dateAdd(date, 5, amount);
  }

  public static Date addHours(Date date, int amount) {
    return dateAdd(date, 11, amount);
  }

  public static Date addMinutes(Date date, int amount) {
    return dateAdd(date, 12, amount);
  }

  public static Date addSeconds(Date date, int amount) {
    return dateAdd(date, 13, amount);
  }

  public static Date addMilliseconds(Date date, int amount) {
    return dateAdd(date, 14, amount);
  }
  private static void validateDateNotNull(Date date) {
    Validate.isTrue(date != null, "The date must not be null", new Object[0]);
  }

  /**
   * 当前时间到当天结束的毫秒数
   * @return long
   */
  public static long getEndTodayMillisecond(){
    //得到今天 23:59:59的日期
    SimpleDateFormat year = new SimpleDateFormat("yyyy-MM-dd");
    String today = year.format(new Date());
    String endTime =today + " 23:59:59";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    long time = 0;
    try {
      time = simpleDateFormat.parse(endTime).getTime();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    //当前时间的毫秒数
    long current = System.currentTimeMillis();
    return time - current;
  }


  public static String formatDuring(long mss) {
    long days = mss / (1000 * 60 * 60 * 24);
    long hours = (mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
    long minutes = (mss % (1000 * 60 * 60)) / (1000 * 60);
    long seconds = (mss % (1000 * 60)) / 1000;
    return days + " days " + hours + " hours " + minutes + " minutes "
            + seconds + " seconds ";
  }

  public static int getYear(Date date){
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return calendar.get(Calendar.YEAR);
  }
  public static int getMonth(Date date){
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return calendar.get(Calendar.MONTH)+1;
  }
  public static int getDay(Date date){
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    return calendar.get(Calendar.DATE);
  }

  public static String getMonthAndDay(Date date){
    int month = getMonth(date);
    int day = getDay(date);
    String m = String.valueOf(month);
    String d = String.valueOf(day);
    if (month < 10){
      m = "0".concat(m);
    }
    if (day < 10){
      d = "0".concat(d);
    }
    return m+d;
  }

  public static int getMonthDiff(Date d1, Date d2) {
    Calendar c1 = Calendar.getInstance();
    Calendar c2 = Calendar.getInstance();
    c1.setTime(d1);
    c2.setTime(d2);
    if(c1.getTimeInMillis() < c2.getTimeInMillis()){
      return 0;
    }
    int year1 = c1.get(Calendar.YEAR);
    int year2 = c2.get(Calendar.YEAR);
    int month1 = c1.get(Calendar.MONTH);
    int month2 = c2.get(Calendar.MONTH);
    int day1 = c1.get(Calendar.DAY_OF_MONTH);
    int day2 = c2.get(Calendar.DAY_OF_MONTH);
    // 获取年的差值 假设 d1 = 2015-8-16 d2 = 2011-9-30
    int yearInterval = year1 - year2;
    // 如果 d1的 月-日 小于 d2的 月-日 那么 yearInterval-- 这样就得到了相差的年数
    if(month1 < month2 || month1 == month2 && day1 < day2){
      yearInterval --;
    }
    // 获取月数差值
    int monthInterval = (month1 + 12) - month2 ;
    if(day1 < day2){
      monthInterval --;
    }
    monthInterval %= 12;
    return yearInterval * 12 + monthInterval;
  }
}