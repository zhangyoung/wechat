package vip.zywork.wechat.pay.util;

import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.UUID;

/**
 * Created date on 2018/10/17
 * Author Zy
 */
@Configuration
@ConfigurationProperties( prefix = "wechat.pay")
public class WXPayConfig extends WXPayConstants {

    //private static String sendBox;
    private static  String key;
    private static  String appId ;
    private static  String mchId ;
    private static String notifyUrl;
    private static String certPath;
//    @Value("${wechat.pay.sendBox}")
//    public void setSendBox(String sendBox) {
//        WXPayConfig.sendBox = sendBox;
//    }
    @Value("${wechat.pay.key:11}")
    public void setKey(String key) {
        this.key = key;
    }
    @Value("${wechat.pay.appId:11}")
    public void setAppId(String appId) {
        this.appId = appId;
    }
    @Value("${wechat.pay.mchId:11}")
    public void setMchId(String mchId) {
        this.mchId = mchId;
    }
    @Value("${wechat.pay.notifyUrl:11}")
    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }
    @Value("${wechat.pay.certPath:11}")
    public void setCertPath(String certPath) {
        this.certPath = certPath;
    }
    public static String getKey() {
        return key;
    }

    public static String getAppId() {
        return appId;
    }

    public static String getMchId() {
        return mchId;
    }

    public static String getNotifyUrl() {
        return notifyUrl;
    }
//    public static String getSendBox() {
//        return sendBox;
//    }
    public static String getCertPath() {
        return certPath;
    }
    public static final String nonce_str = UUID.randomUUID().toString().replaceAll( "\\-","").toUpperCase();
    public static final String CLOSED = "CLOSED";
    public static final String NOTPAY = "NOTPAY";
    public static final String JSAPI = "JSAPI";
    public static final String NATIVE = "NATIVE";//付款类型，JSAPI 公众号支付 ，NATIVE 扫码支付 ，APP APP支付
    public static final String APP = "APP";
    public static final String USER_AGENT =
            " (" + System.getProperty("os.arch") + " " + System.getProperty("os.name") + " " + System.getProperty("os.version") +
                    ") Java/" + System.getProperty("java.version") + " HttpClient/" + HttpClient.class.getPackage().getImplementationVersion();


    //支付成功
    public static final String PAYMENT_SUCCESS = "支付成功";
    //body
    public static final String TOPUPBODY = "充值";
    public static final String BODY ="会员充值";
    //订单已支付错误码
    public static final String ORDERPAID = "ORDERPAID";
    public static final String PLUS_SIGN = "+";
    public static final String MINUS = "-";
    public static final String ZERO = "0";
    public static final String ONE = "1";
    public static final String TWO = "2";
    public static final String THREE = "3";
    public static final String FOUR = "4";
    public static final String FIVE = "5";
    public static final String SIX = "6";
    public static final String TOPUP = "充值";
    public static final String REFUND = "退款";
    public static final String WITHDRAW = "提现";
    public static final String REFUND_ERR_MSG = "充值异常退费";
    public static final String MEMBERTOPUP = "会员充值";

    /**
     * 获取商户证书内容
     *
     * @return 商户证书内容
     */
    protected static InputStream getCertStream() throws FileNotFoundException {

        return new FileInputStream(certPath);
    }

}
