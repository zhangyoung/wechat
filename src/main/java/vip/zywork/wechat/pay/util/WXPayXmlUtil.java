package vip.zywork.wechat.pay.util;

import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.w3c.dom.Document;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created date on 2018/10/17
 * Author Zy
 */
public final class WXPayXmlUtil {
    public static DocumentBuilder newDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
        documentBuilderFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        documentBuilderFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        documentBuilderFactory.setXIncludeAware(false);
        documentBuilderFactory.setExpandEntityReferences(false);

        return documentBuilderFactory.newDocumentBuilder();
    }

    public static Document newDocument() throws ParserConfigurationException {
        return newDocumentBuilder().newDocument();
    }
    public static Map<String,Object> xml2Map(String xmlStr) throws DocumentException {
        org.dom4j.Document document = DocumentHelper.parseText(xmlStr);
        Element root = document.getRootElement();
        return elementTomap(root,new HashMap<>());
    }

    private static Map<String, Object> elementTomap(Element outele, Map<String, Object> outmap) {
        List<Element> list = outele.elements();
        int size = list.size();
        if (size == 0) {
            outmap.put(outele.getName(), outele.getTextTrim());
        } else {
            Map<String, Object> innermap = new HashMap();
            int i = 1;

            for (Element ele1 : list) {
                String eleName = ele1.getName();

                String value = ele1.getText();
                Object obj = innermap.get(eleName);
                if (obj == null) {
                    elementTomap(ele1, innermap);
                } else {
                    if (obj instanceof java.util.Map) {
                        List<Map<String, Object>> list1 = new ArrayList<>();
                        list1.add((Map<String, Object>) innermap.remove(eleName));
                        elementTomap(ele1, innermap);
                        list1.add((Map<String, Object>) innermap.remove(eleName));
                        innermap.put(eleName, list1);
                    } else if (obj instanceof String) {

                        innermap.put(eleName + i, value);
                        i++;
                    } else {
                        elementTomap(ele1, innermap);
                        Map<String, Object> listValue = (Map<String, Object>) innermap.get(eleName);
                        ((List<Map<String, Object>>) obj).add(listValue);
                        innermap.put(eleName, obj);
                    }

                }
            }
            outmap.put(outele.getName(), innermap);
        }
        return outmap;
    }

    public static void main(String[] args) throws DocumentException, IOException {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<settings xmlns=\"http://maven.apache.org/SETTINGS/1.0.0\"\n" +
                "          xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "          xsi:schemaLocation=\"http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd\">\n" +
                "  <localRepository>E:\\Repository</localRepository>\n" +
                "  <proxies>\n" +
                "    <proxy>\n" +
                "      <id>optional</id>\n" +
                "      <active>true</active>\n" +
                "      <protocol>http</protocol>\n" +
                "      <username>proxyuser</username>\n" +
                "      <password>proxypass</password>\n" +
                "      <host>proxy.host.net</host>\n" +
                "      <port>80</port>\n" +
                "      <nonProxyHosts>local.net|some.host.com</nonProxyHosts>\n" +
                "    </proxy>\n" +
                "  </proxies>\n" +
                "  <servers>\n" +
                " <server>\n" +
                "      <id>youngz</id>\n" +
                "      <username>309220728</username>\n" +
                "      <password>Zhyoung!1804</password>\n" +
                "   </server>\n" +
                "  </servers>\n" +
                "  <mirrors>\n" +
                "\t <mirror>  \n" +
                "    <id>nexus-aliyun</id>  \n" +
                "    <mirrorOf>*,!jeecg,!jeecg-snapshots</mirrorOf>    \n" +
                "    <name>Nexus aliyun</name>  \n" +
                "    <url>http://maven.aliyun.com/nexus/content/groups/public</url>  \n" +
                "</mirror>  \n" +
                "<mirror>  \n" +
                "    <id>nexus-aliyun</id>  \n" +
                "    <mirrorOf>central</mirrorOf>    \n" +
                "    <name>Nexus aliyun</name>  \n" +
                "    <url>http://maven.aliyun.com/nexus/content/groups/public</url>  \n" +
                "</mirror> \n" +
                "  </mirrors>\n" +
                "  <profiles>\n" +
                "    <profile>\n" +
                "      <id>jdk-1.4</id>\n" +
                "\n" +
                "      <activation>\n" +
                "        <jdk>1.4</jdk>\n" +
                "      </activation>\n" +
                "\n" +
                "      <repositories>\n" +
                "        <repository>\n" +
                "          <id>jdk14</id>\n" +
                "          <name>Repository for JDK 1.4 builds</name>\n" +
                "          <url>http://www.myhost.com/maven/jdk14</url>\n" +
                "          <layout>default</layout>\n" +
                "          <snapshotPolicy>always</snapshotPolicy>\n" +
                "        </repository>\n" +
                "      </repositories>\n" +
                "    </profile>\n" +
                "  \n" +
                "    <profile>\n" +
                "      <id>env-dev</id>\n" +
                "\n" +
                "      <activation>\n" +
                "        <property>\n" +
                "          <name>target-env</name>\n" +
                "          <value>dev</value>\n" +
                "        </property>\n" +
                "      </activation>\n" +
                "\n" +
                "      <properties>\n" +
                "        <tomcatPath>/path/to/tomcat/instance</tomcatPath>\n" +
                "      </properties>\n" +
                "    </profile>\n" +
                "\n" +
                "  </profiles>\n" +
                "  <activeProfiles>\n" +
                "    <activeProfile>alwaysActiveProfile</activeProfile>\n" +
                "    <activeProfile>anotherAlwaysActiveProfile</activeProfile>\n" +
                "  </activeProfiles>\n" +
                "</settings>\n";

        File file = new File("F://test.txt");
        FileInputStream fileInputStream = new FileInputStream(file);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
        String tempStr;
        StringBuilder sbf = new StringBuilder();
        while ((tempStr = bufferedReader.readLine()) != null) {
            sbf.append(tempStr);
        }
        bufferedReader.close();
        String xml1 = sbf.toString();
        Map<String, Object> stringObjectMap = xml2Map(xml1);

        System.out.println(JsonUtil.objectToJson(stringObjectMap));
    }
}
