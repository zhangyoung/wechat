package vip.zywork.wechat.pay.bean.model;


import lombok.Getter;
import vip.zywork.wechat.pay.core.IWXPayModel;
import vip.zywork.wechat.pay.core.WXPayRequest;
import vip.zywork.wechat.pay.util.WXPayConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * 退费model
 * Created date on 2018/10/18
 * Author Zy
 */
@Getter
public class RefundModel  implements IWXPayModel {

    private String out_trade_no;//商户订单号
    private String out_refund_no;//退款单号
    private String total_fee;//订单金额
    private String refund_fee;//退款金额
    private String refund_desc;//退款原因，可选；
    private String appId;
    private String mchId;
    public Map getMap() {
        Map<Object,Object> map = new HashMap<>();
        map.put("appid",appId);
        map.put("mch_id",mchId);
        map.put("nonce_str", WXPayConfig.nonce_str);
        map.put("out_trade_no",out_trade_no);
        map.put("out_refund_no",out_refund_no);
        map.put("total_fee",total_fee);
        map.put("refund_fee",refund_fee);
        map.put("refund_desc",refund_desc);
        return map;
    }

    public RefundModel(String out_trade_no, String out_refund_no,
                       String total_fee, String refund_fee,
                       String refund_desc,String appId,String mchId) {
        this.out_trade_no = out_trade_no;
        this.out_refund_no = out_refund_no;
        this.total_fee = total_fee;
        this.refund_fee = refund_fee;
        this.refund_desc = refund_desc;
        this.appId = appId;
        this.mchId = mchId;
    }

    @Override
    public WXPayRequest getRequest() {
        WXPayRequest wxPayRequest = new WXPayRequest();
        wxPayRequest.setMap(getMap());
        wxPayRequest.setRequestUrl(WXPayConfig.REFUND_URL);
        return wxPayRequest;
    }

    private RefundModel() {
    }
     public static RefundModel newInstance() {
        return new RefundModel();
    }

    public RefundModel setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
        return this;
    }

    public RefundModel setOut_refund_no(String out_refund_no) {
        this.out_refund_no = out_refund_no;
        return this;
    }

    public RefundModel setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
        return this;
    }

    public RefundModel setRefund_fee(String refund_fee) {
        this.refund_fee = refund_fee;
        return this;
    }

    public RefundModel setRefund_desc(String refund_desc) {
        this.refund_desc = refund_desc;
        return this;
    }

    public RefundModel setAppId(String appId) {
        this.appId = appId;
        return this;
    }

    public RefundModel setMchId(String mchId) {
        this.mchId = mchId;
        return this;
    }
}
