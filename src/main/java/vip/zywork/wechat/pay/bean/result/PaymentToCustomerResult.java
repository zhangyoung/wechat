package vip.zywork.wechat.pay.bean.result;

import lombok.Data;

/**
 * Created date on 2018/11/20
 * Author Zy
 */
@Data
public class PaymentToCustomerResult extends WXResult {

    private String partner_trade_no;//商户订单号
    private String payment_no;//微信付款单号
    private String payment_time;//付款成功时间
    public String getPartner_trade_no() {
        return partner_trade_no;
    }


}
