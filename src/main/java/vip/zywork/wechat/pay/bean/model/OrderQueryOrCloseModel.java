package vip.zywork.wechat.pay.bean.model;


import lombok.Getter;
import vip.zywork.wechat.pay.core.IWXPayModel;
import vip.zywork.wechat.pay.core.WXPayRequest;
import vip.zywork.wechat.pay.util.WXPayConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * 订单查询/关闭订单
 * Created date on 2018/10/19
 * Author Zy
 */
@Getter
public class OrderQueryOrCloseModel implements IWXPayModel {

    private String out_trade_no;
    private String appId;
    private String mchId;
    public Map getMap() {
        Map<Object,Object> map = new HashMap<>();
        map.put("appid",appId);
        map.put("mch_id",mchId);
        map.put("nonce_str", WXPayConfig.nonce_str);
        map.put("out_trade_no",out_trade_no);
        return map;
    }
    public OrderQueryOrCloseModel(String out_trade_no,String appId,String mchId) {
        this.out_trade_no = out_trade_no;
        this.appId = appId;
        this.mchId = mchId;
    }
    @Override
    public WXPayRequest getRequest() {
        WXPayRequest wxPayRequest = new WXPayRequest();
        try {
            wxPayRequest.setMap(getMap());
            wxPayRequest.setRequestUrl(WXPayConfig.ORDERQUERY_URL);
        }catch (Exception e){
            e.printStackTrace();
        }
        return wxPayRequest;
    }
    public WXPayRequest getCloseOrderRequest() {
        WXPayRequest wxPayRequest = new WXPayRequest();
        wxPayRequest.setMap(getMap());
        wxPayRequest.setRequestUrl(WXPayConfig.CLOSEORDER_URL);
        return wxPayRequest;
    }
    private OrderQueryOrCloseModel() {
    }
    public static OrderQueryOrCloseModel newInstance() {
        return new OrderQueryOrCloseModel();
    }

    public OrderQueryOrCloseModel setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
        return this;
    }

    public OrderQueryOrCloseModel setAppId(String appId) {
        this.appId = appId;
        return this;
    }

    public OrderQueryOrCloseModel setMchId(String mchId) {
        this.mchId = mchId;
        return this;
    }
}
