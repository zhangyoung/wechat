package vip.zywork.wechat.pay.bean.model;


import vip.zywork.wechat.pay.core.IWXPayModel;
import vip.zywork.wechat.pay.core.WXPayRequest;
import vip.zywork.wechat.pay.util.WXPayConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * Created date on 2018/11/23
 * Author Zy
 */
public class SendBoxSignModel  implements IWXPayModel {

    public Map getMap() {
        Map<Object,Object> map = new HashMap<>();
        map.put("mch_id", WXPayConfig.getMchId());
        map.put("nonce_str",WXPayConfig.nonce_str);
        return map;
    }
    public SendBoxSignModel() {
    }

    @Override
    public WXPayRequest getRequest() {
        Map map = getMap();
        WXPayRequest wxPayRequest = new WXPayRequest();
        wxPayRequest.setMap(map);
        wxPayRequest.setRequestUrl(WXPayConfig.SANDBOX_GETSIGNKEY_URL);//沙箱
        return wxPayRequest;
    }
}
