package vip.zywork.wechat.pay.bean.result;

import lombok.Data;

/**
 * Created date on 2018/11/6
 * Author Zy
 */
@Data
public class OrderQueryResult extends WXResult{

    private String openid;
    private String is_subscribe;
    private String trade_type;
    private String trade_state;
    private String bank_type;
    private String total_fee;
    private String settlement_total_fee;
    private String coupon_fee;
    private String coupon_count;
    private String coupon_type;
    private String coupon_id;
    private String coupon_fee_one;
    private String transaction_id;
    private String out_trade_no;
    private String time_end;
    private String trade_state_desc;
    private String attach;


}
