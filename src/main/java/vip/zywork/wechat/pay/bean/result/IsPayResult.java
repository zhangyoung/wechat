package vip.zywork.wechat.pay.bean.result;

import lombok.Data;

/**
 * Created date on 2018/12/10
 * Author Zy
 */
@Data
public class IsPayResult extends OrderQueryResult {

    private String cash_fee;


}
