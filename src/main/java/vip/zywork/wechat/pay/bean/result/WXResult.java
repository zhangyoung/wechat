package vip.zywork.wechat.pay.bean.result;

import lombok.Data;

/**
 * 接口返回结果
 * Created date on 2018/10/29
 * Author Zy
 */
@Data
public class WXResult {
    private String return_code;//返回状态码
    private String return_msg;//返回信息
    private String appid;//公众账号ID
    private String mch_id;//商户号
    private String nonce_str;//随机字符串
    private String sign;//签名
    private String result_code;//业务结果
    private String err_code;//错误代码
    private String err_code_des;//错误代码描述
    private String openId;


}
