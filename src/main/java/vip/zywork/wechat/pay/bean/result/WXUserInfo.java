package vip.zywork.wechat.pay.bean.result;

import lombok.Data;

/**
 * 微信用户详情
 * Created date on 2018/10/22
 * Author Zy
 */
@Data
public class WXUserInfo {
    private String openid;
    private String nickname;
    private String sex;
    private String province;
    private String city;
    private String country;
    private String headimgurl;
    private String[] privilege;
    private String unionid;
    private String errcode;
    private String errmsg;


}
