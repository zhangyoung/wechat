package vip.zywork.wechat.pay.bean.model;


import lombok.Getter;
import vip.zywork.wechat.pay.core.IWXPayModel;
import vip.zywork.wechat.pay.core.WXPayRequest;
import vip.zywork.wechat.pay.util.WXPayConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * 企业付款
 * Created date on 2018/10/25
 * Author Zy
 */
@Getter
public class PaymentToCustomerModel  implements IWXPayModel {

    public static final String mchid = WXPayConfig.getMchId();//商户号
    private String partner_trade_no;//商户订单号
    private String openid;//用户openid
    private String check_name;//校验用户姓名选项，NO_CHECK：不校验真实姓名，FORCE_CHECK：强校验真实姓名
    private String amount;//付款金额
    private String desc;//企业付款备注
    private String spbill_create_ip;//ip地址
    private String appId;
    public Map getMap() {
        Map<Object,Object> map = new HashMap<>();
        map.put("mch_appid",appId);
        map.put("mchid",mchid);
        map.put("nonce_str",WXPayConfig.nonce_str);
        map.put("partner_trade_no",partner_trade_no);
        map.put("openid",openid);
        map.put("check_name",check_name);
        map.put("amount",amount);
        map.put("desc",desc);
        map.put("spbill_create_ip",spbill_create_ip);
        return map;
    }

    public PaymentToCustomerModel(String partner_trade_no,String openid,
                                  String check_name, String amount,
                                  String desc, String spbill_create_ip,String appId) {
        this.partner_trade_no = partner_trade_no;
        this.openid = openid;
        this.check_name = check_name;
        this.amount = amount;
        this.desc = desc;
        this.spbill_create_ip = spbill_create_ip;
        this.appId = appId;
    }
    @Override
    public WXPayRequest getRequest() {
        WXPayRequest wxPayRequest = new WXPayRequest();
        wxPayRequest.setMap(getMap());
        wxPayRequest.setRequestUrl(WXPayConfig.PAYMENTTOCUSTOMER_URL);
        return wxPayRequest;
    }
    private PaymentToCustomerModel() {
    }
    public static PaymentToCustomerModel newInstance() {
        return new PaymentToCustomerModel();
    }

    public PaymentToCustomerModel setPartner_trade_no(String partner_trade_no) {
        this.partner_trade_no = partner_trade_no;
        return this;
    }

    public PaymentToCustomerModel setOpenid(String openid) {
        this.openid = openid;
        return this;
    }

    public PaymentToCustomerModel setCheck_name(String check_name) {
        this.check_name = check_name;
        return this;
    }

    public PaymentToCustomerModel setAmount(String amount) {
        this.amount = amount;
        return this;
    }

    public PaymentToCustomerModel setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public PaymentToCustomerModel setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
        return this;
    }

    public PaymentToCustomerModel setAppId(String appId) {
        this.appId = appId;
        return this;
    }
}
