package vip.zywork.wechat.pay.bean.result;

import lombok.Data;

import java.util.Map;

/**
 * Created date on 2018/10/17
 * Author Zy
 */
@Data
public class WXPayResult extends WXResult {

    private String trade_type;//交易类型
    private String prepay_id;//预支付交易会话标识
    private String code_url;//二维码链接
    private Map<String,String> map;
    public WXPayResult(Map<String,String> map){
        this.map = map;
    }
    public WXPayResult(){}


}
