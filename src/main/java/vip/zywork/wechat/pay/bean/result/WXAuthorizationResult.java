package vip.zywork.wechat.pay.bean.result;

import lombok.Data;

/**
 * WX网页授权model
 * Created date on 2018/10/22
 * Author Zy
 */
@Data
public class WXAuthorizationResult {
    private String access_token;//接口调用凭证
    private String expires_in;//access_token接口调用凭证超时时间，单位（秒）
    private String refresh_token;//用户刷新access_token
    private String openid;//授权用户唯一标识
    private String scope;//用户授权的作用域，使用逗号（,）分隔
    private String errcode;//
    private String errmsg;//


}
