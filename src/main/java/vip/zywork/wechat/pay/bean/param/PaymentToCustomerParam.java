package vip.zywork.wechat.pay.bean.param;

import lombok.Data;

/**
 * Created date on 2018/11/20
 * Author Zy
 */
@Data
public class PaymentToCustomerParam {
    private String partner_trade_no;
    private String openId;
}
