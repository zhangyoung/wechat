package vip.zywork.wechat.pay.bean.result;

import lombok.Data;

/**
 * Created date on 2018/12/5
 * Author Zy
 */
@Data
public class RefundResult extends WXResult {

    private String transaction_id;//微信订单号
    private String out_refund_no;//商户退款单号
    private String refund_id;//微信退款单号
    private String refund_fee;//退款金额
    private String settlement_refund_fee;//应结退款金额
    private String total_fee;//标价金额
    private String settlement_total_fee;//应结订单金额
    private String fee_type;//标价币种
    private String cash_fee;//现金支付金额
    private String cash_fee_type;//现金支付币种
    private String cash_refund_fee;//现金退款金额
    private String coupon_refund_fee;//代金券退款总金额

}
