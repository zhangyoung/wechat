package vip.zywork.wechat.pay.bean.result;



import lombok.Data;
import org.springframework.util.StringUtils;
import vip.zywork.wechat.pay.util.WXPayConfig;
import vip.zywork.wechat.pay.util.WXPayUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created date on 2018/11/8
 * Author Zy
 */
@Data
public class UnifiedOrderResult  {
    private String out_trade_no;
    private String code_url;
    private String prepay_id;//预支付交易会话标识
    private String packages;
    private String timeStamp;
    private String appId;
    private String nonceStr;
    private String signType;
    private String paySign;
    private String mch_id;//商户号
    private String return_code;//返回状态码
    private String return_msg;//返回信息
    private String result_code;//业务结果
    private String err_code;//错误代码
    private String err_code_des;//错误代码描述
    public UnifiedOrderResult(){
    }

    public UnifiedOrderResult(String prepay_id) throws Exception {
        long timeStampSec = System.currentTimeMillis()/1000;
        String timestamp = String.format("%010d", timeStampSec);
        this.timeStamp = timestamp;
        this.appId = WXPayConfig.getAppId();
        this.nonceStr = WXPayConfig.nonce_str;
        this.signType = WXPayConfig.MD5;
        this.packages = "prepay_id=" + prepay_id;
        this.prepay_id = prepay_id;
        this.paySign = getAppletSign();
    }
    public UnifiedOrderResult(String prepay_id,String appId) throws Exception {
        long timeStampSec = System.currentTimeMillis()/1000;
        String timestamp = String.format("%010d", timeStampSec);
        this.timeStamp = timestamp;
        this.appId = appId;
        this.nonceStr = WXPayConfig.nonce_str;
        this.signType = WXPayConfig.MD5;
        this.packages = "prepay_id=" + prepay_id;
        this.prepay_id = prepay_id;
        this.paySign = getAppletSign();
    }
    public UnifiedOrderResult(String prepay_id,String appId,String mch_id) throws Exception {
        long timeStampSec = System.currentTimeMillis()/1000;
        String timestamp = String.format("%010d", timeStampSec);
        this.timeStamp = timestamp;
        this.appId = appId;
        this.nonceStr = WXPayConfig.nonce_str;
        this.signType = WXPayConfig.MD5;
        this.packages = "Sign=WXPay";
        this.mch_id = mch_id;
        this.prepay_id = prepay_id;
        this.paySign = getAppSign();
    }
    private String getAppletSign() throws Exception {
        Map<String,String> map = new HashMap<>();
        map.put("appId",appId);
        map.put("nonceStr",nonceStr);
        map.put("package",packages);
        map.put("signType",signType);
        map.put("timeStamp",timeStamp);
        return WXPayUtil.generateSignature(map,WXPayConfig.getKey());
    }

    private String getAppSign() throws Exception {
        Map<String,String> map = new HashMap<>();
        map.put("appid",appId);
        map.put("partnerid",mch_id);
        map.put("prepayid",prepay_id);
        map.put("package",packages);
        map.put("noncestr",nonceStr);
        map.put("timestamp",timeStamp);
        return WXPayUtil.generateSignature(map,WXPayConfig.getKey());
    }

}
