package vip.zywork.wechat.pay.bean.model;




import lombok.Getter;
import vip.zywork.wechat.pay.bean.param.RequestParam;
import vip.zywork.wechat.pay.core.IWXPayModel;
import vip.zywork.wechat.pay.core.WXPayRequest;
import vip.zywork.wechat.pay.util.IpUtil;
import vip.zywork.wechat.pay.util.WXPayConfig;
import org.springframework.util.StringUtils;
import vip.zywork.wechat.pay.util.WXPayUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 下单请求model
 * Created date on 2018/10/17
 * Author Zy
 */
@Getter
public class UnifiedOrderModel implements IWXPayModel {

    private String body;//商品参数信息
    private String out_trade_no;//订单号
    private String spbill_create_ip;//请求IPrequest.getRemoteAddr()
    private String total_fee;//订单金额：单位分
    private String trade_type;//付款类型，JSAPI 公众号支付 ，NATIVE 扫码支付 ，APP APP支付
    private String sign;//签名
    private String appId;
    private String openId;
    private String notifyUrl;//回调页面
    private String mchId;
    private String timeStart;
    private String timeExpire;
    private String goodsTag;
    private String detail;
    private String attach;
    private String receipt;
    private String limitPay;
    public Map getMap() {
        if (StringUtils.isEmpty(appId)){
            this.appId = WXPayConfig.getAppId();
        }
        if (StringUtils.isEmpty(spbill_create_ip)){
            this.spbill_create_ip = IpUtil.getIpAddr(new RequestParam().getRequest());
        }
        if (StringUtils.isEmpty(body)){
            this.body = WXPayConfig.BODY;
        }
        if (StringUtils.isEmpty(notifyUrl)){
            this.notifyUrl = WXPayConfig.getNotifyUrl();
        }
        if (StringUtils.isEmpty(mchId)){
            this.mchId = WXPayConfig.getMchId();
        }
        Map<Object,Object> map = new HashMap<>();
        map.put("appid",appId);
        map.put("mch_id",mchId);
        map.put("notify_url",notifyUrl);
        map.put("nonce_str", WXPayUtil.generateNonceStr());
        map.put("body",body);
        map.put("out_trade_no",out_trade_no);
        map.put("spbill_create_ip",spbill_create_ip);
        map.put("total_fee",total_fee);
        map.put("trade_type",trade_type);
        if (!StringUtils.isEmpty(openId)){
            map.put("openid",openId);
        }
        if (!StringUtils.isEmpty(timeStart)){
            map.put("time_start",timeStart);
        }
        if (!StringUtils.isEmpty(timeExpire)){
            map.put("time_expire",timeExpire);
        }
        if (!StringUtils.isEmpty(goodsTag)){
            map.put("goods_tag",goodsTag);
        }
        if (!StringUtils.isEmpty(detail)){
            map.put("detail",detail);
        }
        if (!StringUtils.isEmpty(attach)){
            map.put("attach",attach);
        }
        if (!StringUtils.isEmpty(receipt)){
            map.put("receipt",receipt);
        }
        if (!StringUtils.isEmpty(limitPay)){
            map.put("limit_pay",limitPay);
        }
        return map;
    }
    public static UnifiedOrderModel newInstance(){
        return new UnifiedOrderModel();
    }

    private UnifiedOrderModel() {
    }

    @Override
    public WXPayRequest getRequest(){
        WXPayRequest wxPayRequest = new WXPayRequest();
        wxPayRequest.setMap(getMap());
        wxPayRequest.setRequestUrl(WXPayConfig.UNIFIEDORDER_URL);//正式
//        wxPayRequest.setRequestUrl(WXPayConfig.SANDBOX_UNIFIEDORDER_URL);//沙箱
        return wxPayRequest;
    }


    public UnifiedOrderModel(String body, String out_trade_no,
                             String spbill_create_ip, String total_fee,
                             String trade_type, String appId) {
        this.body = body;
        this.out_trade_no = out_trade_no;
        this.spbill_create_ip = spbill_create_ip;
        this.total_fee = total_fee;
        this.trade_type = trade_type;
        this.appId = appId;
    }

    public UnifiedOrderModel(String body, String notifyUrl, String mchId, String out_trade_no,
                             String spbill_create_ip, String total_fee,
                             String trade_type, String appId, String openId) {
        this.body = body;
        this.notifyUrl = notifyUrl;
        this.mchId = mchId;
        this.out_trade_no = out_trade_no;
        this.spbill_create_ip = spbill_create_ip;
        this.total_fee = total_fee;
        this.trade_type = trade_type;
        this.appId = appId;
        this.openId = openId;
    }

    public UnifiedOrderModel setBody(String body) {
        this.body = body;
        return this;
    }

    public UnifiedOrderModel setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
        return this;
    }

    public UnifiedOrderModel setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
        return this;
    }

    public UnifiedOrderModel setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
        return this;
    }

    public UnifiedOrderModel setTrade_type(String trade_type) {
        this.trade_type = trade_type;
        return this;
    }

    public UnifiedOrderModel setSign(String sign) {
        this.sign = sign;
        return this;
    }

    public UnifiedOrderModel setAppId(String appId) {
        this.appId = appId;
        return this;
    }

    public UnifiedOrderModel setOpenId(String openId) {
        this.openId = openId;
        return this;
    }

    public UnifiedOrderModel setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
        return this;
    }

    public UnifiedOrderModel setMchId(String mchId) {
        this.mchId = mchId;
        return this;
    }

    public UnifiedOrderModel setTimeStart(String timeStart) {
        this.timeStart = timeStart;
        return this;
    }

    public UnifiedOrderModel setTimeExpire(String timeExpire) {
        this.timeExpire = timeExpire;
        return this;
    }

    public UnifiedOrderModel setGoodsTag(String goodsTag) {
        this.goodsTag = goodsTag;
        return this;
    }

    public UnifiedOrderModel setDetail(String detail) {
        this.detail = detail;
        return this;
    }

    public UnifiedOrderModel setAttach(String attach) {
        this.attach = attach;
        return this;
    }

    public UnifiedOrderModel setReceipt(String receipt) {
        this.receipt = receipt;
        return this;
    }

    public UnifiedOrderModel setLimitPay(String limitPay) {
        this.limitPay = limitPay;
        return this;
    }
}
