package vip.zywork.wechat.pay.bean.model;


import lombok.Getter;
import vip.zywork.wechat.pay.core.IWXPayModel;
import vip.zywork.wechat.pay.core.WXPayRequest;
import vip.zywork.wechat.pay.util.WXPayConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * 查询企业付款model
 * Created date on 2018/10/26
 * Author Zy
 */
@Getter
public class QueryPaymentModel implements IWXPayModel {
    private String partner_trade_no;
    private String appId;
    public Map getMap() {
        Map<Object,Object> map = new HashMap<>();
        map.put("appid",appId);
        map.put("mch_id", WXPayConfig.getMchId());
        map.put("nonce_str",WXPayConfig.nonce_str);
        map.put("partner_trade_no",partner_trade_no);
        return map;
    }

    public QueryPaymentModel(String partner_trade_no,String appId) {
        this.partner_trade_no = partner_trade_no;
        this.appId = appId;
    }

    @Override
    public WXPayRequest getRequest() {
        WXPayRequest wxPayRequest = new WXPayRequest();
        wxPayRequest.setMap(getMap());
        wxPayRequest.setRequestUrl(WXPayConfig.PAYMENQUERY_URL);
        return wxPayRequest;
    }
    private QueryPaymentModel() {
    }
    public static QueryPaymentModel newInstance() {
        return new QueryPaymentModel();
    }

    public QueryPaymentModel setPartner_trade_no(String partner_trade_no) {
        this.partner_trade_no = partner_trade_no;
        return this;
    }

    public QueryPaymentModel setAppId(String appId) {
        this.appId = appId;
        return this;
    }
}
