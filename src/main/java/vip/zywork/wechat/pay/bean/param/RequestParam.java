package vip.zywork.wechat.pay.bean.param;

import lombok.Data;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;

/**
 * 微信支付下单支付请求参数
 * Created date on 2018/11/16
 * Author Zy
 */
@Data
public class RequestParam {
    private Double unitMoney;//单位元
    private String money;//金额
    private String sendType;//请求终端类型
    private String tradeNo;//商户订单号
    private String openId;//商户下用户的唯一标示
    private String paymentType;//支付的场景 1充值，2预支付商单
    private String productId;//商品ID
    private Integer orderAmount;//商品下单数量
    private BigDecimal orderTotalPrice;//商品总价
    private Integer orderType; //订单类型
    private Integer orderDistributorId;//分销商ID

    /**
     * 获取Request
     * @return HttpServletRequest
     */
    public HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 获取Response
     * @return HttpServletRequest
     */
    public HttpServletResponse getResponse() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

}
