package vip.zywork.wechat.pay.core;


import vip.zywork.wechat.pay.bean.model.*;
import vip.zywork.wechat.pay.bean.param.RequestParam;

/**
 * Created date on 2018/11/19
 * Author Zy
 */
public interface IWXPayModelFactory {

    OrderQueryOrCloseModel createOrderQueryOrCloseModel(RequestParam requestParam);
    UnifiedOrderModel createUnifiedOrderModel(RequestParam requestParam);
    PaymentToCustomerModel createPaymentToCustomerModel(RequestParam requestParam);
    SendBoxSignModel createSendBoxSignModel();
    RefundModel createRefundModel(RequestParam requestParam);
    <T> T  createModel(Class<T> clazz, RequestParam requestParam) throws IllegalAccessException, InstantiationException;
    WXPayRequest  createModel(IWXPayModel model);
}
