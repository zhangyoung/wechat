package vip.zywork.wechat.pay.core;

import java.util.Map;

/**
 * Created date on 2018/11/19
 * Author Zy
 */
public class WXPayRequest {
    private Map<String,String> map;
    private String requestUrl;

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }
}
