package vip.zywork.wechat.pay.core;


import vip.zywork.wechat.pay.util.HttpUtils;
import vip.zywork.wechat.pay.util.JsonUtil;
import vip.zywork.wechat.pay.util.WXPayConfig;
import vip.zywork.wechat.pay.util.WXPayUtil;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created date on 2018/11/19
 * Author Zy
 */
@Component
public class WXPayClient implements IWXPayClient {
//    private WXPayClient wxPayClient = null;
//    private WXPayClient(){}
//    public WXPayClient getInstance(){
//        if (null == wxPayClient){
//            synchronized (WXPayClient.class){
//                if (null == wxPayClient){
//                    wxPayClient = new WXPayClient();
//                }
//            }
//        }
//        return wxPayClient;
//    }

    /**
     * 获取接口返回的map结果集
     * @param wxPayRequest
     * @return Map获取接口返回的map结果集
     * @throws Exception 异常
     */
    public Map<String,String> getResultMap(WXPayRequest wxPayRequest) throws Exception {
        String paramXml = WXPayUtil.generateSignedXml(wxPayRequest.getMap(), WXPayConfig.getKey(), WXPayConfig.SignType.MD5);
        String result = HttpUtils.httpsRequest(wxPayRequest.getRequestUrl(), HttpUtils.RequestMethod.POST,paramXml);
        return WXPayUtil.xmlToMap(result);
    }
    /**
     * 获取带证书接口返回的map结果集
     * @param wxPayRequest
     * @return Map 获取带证书接口返回的map结果集
     * @throws Exception 异常
     */
    public Map<String,String> getResultMapByCert(WXPayRequest wxPayRequest) throws Exception {
        String paramXml = WXPayUtil.generateSignedXml(wxPayRequest.getMap(), WXPayConfig.getKey(), WXPayConfig.SignType.MD5);
        String result =  HttpUtils.requestOnce(wxPayRequest.getRequestUrl(),paramXml,30000,100000,true);
        return WXPayUtil.xmlToMap(result);
    }

    @Override
    public <T> T execute(WXPayRequest wxPayRequest, Class<T> resultClass) {

        Map<String,String> map = null;
        try {
            map = getResultMap(wxPayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JsonUtil.jsonToObject(JsonUtil.objectToJson(map), resultClass);
    }
    @Override
    public Map<String,String> execute(WXPayRequest wxPayRequest) {

        Map<String,String> map = null;
        try {
            map = getResultMap(wxPayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
    @Override
    public <T> T createWXPayResult(Map<String, String> paramMap, String requestUrl, Class<T> resultClass) {
       return null;
    }

    @Override
    public <T> T executeByCert(WXPayRequest wxPayRequest, Class<T> resultClass) {
        Map<String,String> map = null;
        try {
            map = getResultMapByCert(wxPayRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JsonUtil.jsonToObject(JsonUtil.objectToJson(map), resultClass);
    }
}
