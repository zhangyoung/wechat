package vip.zywork.wechat.pay.core;


import vip.zywork.wechat.pay.bean.model.*;
import vip.zywork.wechat.pay.bean.param.RequestParam;
import vip.zywork.wechat.pay.util.DateUtil;
import vip.zywork.wechat.pay.util.IpUtil;
import vip.zywork.wechat.pay.util.WXPayConfig;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import vip.zywork.wechat.pay.util.WXPayUtil;

import java.util.Date;

/**
 * Created date on 2018/11/19
 * Author Zy
 */
@Component
public class WXPayModelFactory implements IWXPayModelFactory {




    /**
     * 查询或关闭订单
     * @param requestParam 请求参数
     * @return OrderQueryOrCloseModel
     */
    @Override
    public OrderQueryOrCloseModel createOrderQueryOrCloseModel(RequestParam requestParam) {
        return new OrderQueryOrCloseModel(requestParam.getTradeNo(), WXPayConfig.getAppId(),WXPayConfig.getMchId());
    }


    @Override
    public WXPayRequest createModel(IWXPayModel model) {
        return model.getRequest();
    }

    public WXPayRequest getWXPayRequest(RequestParam requestParam){




        return createModel(createUnifiedOrderModel(requestParam));
    }
    /**
     * 下单
     * @param requestParam 请求参数
     * @return UnifiedOrderModel
     */
    @Override
    public UnifiedOrderModel createUnifiedOrderModel(RequestParam requestParam) {
        String appId = WXPayConfig.getAppId();
        String openId = requestParam.getOpenId();
        String trade_type = getTradeType(requestParam.getSendType());
        String ip = IpUtil.getIpAddr(requestParam.getRequest());
        return new UnifiedOrderModel(WXPayConfig.BODY,WXPayConfig.getNotifyUrl(),WXPayConfig.getMchId(),
                requestParam.getTradeNo(),ip,requestParam.getMoney(),trade_type,appId,openId);
    }

    /**
     * 企业付款到零钱
     * @param requestParam 请求参数
     * @return PaymentToCustomerModel
     */
    @Override
    public PaymentToCustomerModel createPaymentToCustomerModel(RequestParam requestParam) {
        String appId = WXPayConfig.getAppId();
        String check_name = "NO_CHECK";
        String ip = IpUtil.getIpAddr(requestParam.getRequest());
        String desc = "提现";
        return new PaymentToCustomerModel(requestParam.getTradeNo()
                ,requestParam.getOpenId(),check_name,requestParam.getMoney(),desc,ip,appId);
    }

    @Override
    public SendBoxSignModel createSendBoxSignModel() {


        return new SendBoxSignModel();
    }


    /**
     * 退费
     * @param requestParam 请求参数
     * @return RefundModel
     */
    @Override
    public RefundModel createRefundModel(RequestParam requestParam) {
        String appId = WXPayConfig.getAppId();
        String out_trade_no = requestParam.getTradeNo();
        String out_refund_no = WXPayUtil.getOrderIdByUUId(1) + DateUtil.formatDateTime(new Date(),"yyyyMMddHHmmss");//退款单号
        return new RefundModel(out_trade_no,out_refund_no,
                requestParam.getMoney(),
                requestParam.getMoney(),WXPayConfig.REFUND_ERR_MSG,appId,WXPayConfig.getMchId());
    }

    @Override
    public <T> T createModel(Class<T> clazz, RequestParam requestParam) throws IllegalAccessException, InstantiationException {
        return null;
    }

    /**
     *
     * @param sendType 请求类型
     * @return String
     */
    public String getTradeType(String sendType){
        if (StringUtils.isEmpty(sendType)){
            return null;
        }
        //PC端
        if (sendType.equals(WXPayConfig.ONE)){
            return WXPayConfig.NATIVE;
        }
        //小程序
        if (sendType.equals(WXPayConfig.TWO)){
            return WXPayConfig.JSAPI;
        }
        //小程序
        if (sendType.equals(WXPayConfig.THREE)){
            return WXPayConfig.JSAPI;
        }
        return sendType;
    }
}
