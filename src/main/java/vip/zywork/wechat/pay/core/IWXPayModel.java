package vip.zywork.wechat.pay.core;

import java.util.Map;

/**
 * Created date on 2018/11/20
 * Author Zy
 */
public interface IWXPayModel {
    WXPayRequest getRequest();
    Map<String,String> getMap() throws Exception;
}
