package vip.zywork.wechat.pay.core;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


/**
 * Created date on 2021/1/3 14:46
 * Author Zy
 */
@Component
@ConfigurationProperties(prefix = "spring.zywork.wechat-pay")
public class WeChatPayProperties {
    private boolean enable;
    public boolean isEnable() {
        return enable;
    }
    public void setEnable(boolean enable) {
        this.enable = enable;
    }
}
