package vip.zywork.wechat.pay.core;

import java.util.Map;

/**
 * Created date on 2018/11/19
 * Author Zy
 */
public interface IWXPayClient {
    <T> T execute(WXPayRequest wxPayRequest, Class<T> resultClass);
    Map<String,String> execute(WXPayRequest wxPayRequest);
    <T> T createWXPayResult(Map<String, String> paramMap, String requestUrl, Class<T> resultClass);
    <T> T executeByCert(WXPayRequest wxPayRequest, Class<T> resultClass);
}
